import pandas as pd
import numpy as np
from collections import defaultdict
import sys, os

def preprocessing(df):
    cond_o = (df.dtypes=='object').values
    cond_i = (df.dtypes=='int64').values
    cond_f = (df.dtypes=='float64').values
    cond_d = (df.dtypes=='datetime64[ns]').values
    
    df.loc[:, cond_d] = df.loc[:, cond_d].fillna(pd.NaT)
    df.loc[:, cond_f] = df.loc[:, cond_f].fillna(0).astype(int)
    df.loc[:, cond_i] = df.loc[:, cond_i].fillna(0)
    df.loc[:, cond_o] = df.loc[:, cond_o].fillna('空白')
    
    return df


if __name__=='__main__':
    UPLOAD_BASE = '/tmp/'
    
    args = sys.argv
    file_uuid = args[1]
    books = pd.ExcelFile(os.path.join(UPLOAD_BASE, file_uuid, 'original.xlsx'))
    df = books.parse("注文全件データ")
    df = preprocessing(df)

    df.to_pickle(os.path.join(UPLOAD_BASE, file_uuid, 'original.pickle'))

    


