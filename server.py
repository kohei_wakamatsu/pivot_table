import os
from flask import Flask, request, redirect, session, jsonify
from flask import render_template, url_for, send_from_directory, send_file
import pandas as pd
import numpy as np
import uuid, os, pdb, io, time
from glob import glob
from subprocess import Popen 
from backend import *

UPLOAD_BASE = '/tmp/'
ALLOWED_EXTENSIONS = set(['csv', 'xlsx', 'txt'])

app = Flask(__name__)
app.secret_key = 'hogehoge'
app.config['UPLOAD_BASE'] = UPLOAD_BASE
app.config['JSON_AS_ASCII'] = False


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


def loading_file():
    return 0


@app.route('/')
def upload_file():
    return render_template('upload.html')


@app.route('/', methods=['POST'])
def select_columns():
    file = request.files['file']

    if file and allowed_file(file.filename):
        session['uuid'] = str(uuid.uuid4())
        session['dir_path'] = os.path.join(app.config['UPLOAD_BASE'], session['uuid'])
        os.mkdir(session['dir_path'])

        file_ex = file.filename.rsplit('.')[-1]
        if(file_ex == 'xlsx'):
            file.save(os.path.join(session.get('dir_path'), 'original.xlsx'))
            ps = Popen(['python', 'excel_to_pickle.py', session.get('uuid')])
        else:
            file.save(os.path.join(session.get('dir_path'), 'original.csv'))
            ps = Popen(['python', 'csv_to_pickle.py', session.get('uuid')])
       
        while(ps.poll() is None):
            time.sleep(0.25)

        df = pd.read_pickle(os.path.join(session.get('dir_path'), 'original.pickle'))
        columns = df.columns.values
        unique_dict = get_unique_dict(df)
        
    return render_template('select_columns.html', columns=columns,
                            origin_columns=columns, unique_dict=unique_dict)


@app.route('/view_table', methods=['GET', 'POST'])
def view_table():
    df = pd.read_pickle(os.path.join(session.get('dir_path'), 'original.pickle'))
    origin_columns = df.columns.values
    unique_dict = get_unique_dict(df)
    type_dict = get_type_dict(df)

    fil_key = request.form.get('filter_key')
    if(fil_key != 'None'):
        fil_val = change_type(request.form.get('filter_value'), type_dict[fil_key])
        if(fil_val != 'None'):
            df = filtering_by_category(df, fil_key, fil_val)

    v_cond = request.form.get('v_cond')
    h_cond = request.form.get('h_cond')
    ex_df = extract_table(df, v_cond, h_cond)
    csv_path = os.path.join(session.get('dir_path'), 'pivot.csv')
    ex_df.to_csv(csv_path, encoding='shift-jis')
    indexes = ex_df.index.values
    columns = ex_df.columns.values
    values  = ex_df.values
    return render_template('view_table.html', uuid=session.get('uuid'),
                            v_cond=v_cond, h_cond=h_cond, origin_columns=origin_columns, unique_dict=unique_dict,
                            indexes=indexes, columns=columns, values=values)

@app.route('/download')
def download_file():
    return send_file(os.path.join(session['dir_path'], 'pivot.csv'), \
                     mimetype="text/csv; charset=Shift_JIS")



if __name__=='__main__':
    app.run(debug=True, host='0.0.0.0', threaded=True)
