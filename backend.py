import pandas as pd
import numpy as np
from collections import defaultdict
import sys, os


def extract_table(df, v_cond, h_cond):
    v_keys = list(df.groupby(v_cond).groups.keys())
    h_keys = list(df.groupby(h_cond).groups.keys())

    v_groups = df.groupby(v_cond)
    vh_df = pd.DataFrame(columns=h_keys)

    for v_key, v_group in v_groups:
        h_total = 0
        h_groups = v_group.groupby(h_cond)
        h_data_dict = dict(zip(h_keys, [0 for _ in h_keys]))

        for h_key, h_group in h_groups:
            vh_num = len(h_group)
            h_data_dict[h_key] = vh_num

        tmp_df = pd.DataFrame([h_data_dict])
        vh_df = vh_df.append(tmp_df)

    vh_df.index = v_keys
    return vh_df


def get_percentile(df, axis=0):
    percentile_df = pd.DataFrame()

    if(axis==0):
        for i, index in enumerate(df.index):
            tmp_df = df.loc[index, :] / df.sum(axis=1).iloc[i] * 100
            percentile_df = percentile_df.append(tmp_df)

    elif(axis==1):
        tmp_df = pd.DataFrame(columns=df.columns)
        for i, colum in enumerate(df.columns):
            tmp_df[colum] = df.loc[:, colum] / df.sum(axis=0).iloc[i] * 100
        percentile_df = tmp_df

    else:
        print("None of params axis={0:2d}".format(axis))
        percentile_df = df

    return percentile_df


def add_total_lines(df, axis=0):
    df.loc['col_total'] = df.sum(axis=0)
    df['row_total'] = df.sum(axis=1)

    if(axis==0):
        df['row_total'] = df['row_total'] / df['row_total'].iloc[:-1].sum() * 100
    elif(axis==1):
        df.loc['col_total'] = df.sum(axis=0) / df.sum(axis=0)[:-1].sum() * 100

    return df


def filtering_by_category(df, column, key):
    return df.loc[df[column]==key, :]


def filtering_by_numerious(df, column, condition_func):
    cond_df = df[column].apply(condition_func)
    ex_index = cond_df[cond_df==1].index.values
    return df.loc[ex_index, :]


def get_unique_dict(df):
    unique_dict = {}

    for key in df.columns:
        key_counts = df[key].value_counts()
        if(key_counts.shape[0] < 30):
            try:
                tmp_arr = np.sort(key_counts.index.values)
            except:
                tmp_arr = key_counts.index.values
            unique_dict[key] = tmp_arr
        else:
            unique_dict[key] = np.array(['Unselectable'])

    return unique_dict


def change_type(obj, o_type):
    if(o_type=='int64'):
        return int(obj)
    elif(o_type=='datetime64[ns]'):
        return pd.to_datetime(obj)


def get_type_dict(df):
    return {k:str(v) for k, v in df.dtypes.items()}

    